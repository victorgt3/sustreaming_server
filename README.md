# sustreaming_server

A NodeJS server to handle video call messages.

This project depends of:

- redis server
- twilio auth token and account SID

To start the project you need to execute:

- `node server/server.js`

If you need to push the project to production environment you need to set a certificate.
