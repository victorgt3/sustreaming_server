#!/usr/bin/env node
const dotenv = require('dotenv');
dotenv.config();

const HTTPS_PORT = process.env.HTTPS_PORT || 8443; //default port for https is 443

const fs = require('fs');
const https = require('https');
const WebSocket = require('ws');
const redis = require("redis");
const crypto = require('crypto');

// Crypto configuration
const cryptoConfig = {
  algorithm: 'aes-256-cbc',
  key: crypto.randomBytes(32),
  iv: crypto.randomBytes(16)
};

// based on examples at https://www.npmjs.com/package/ws
const WebSocketServer = WebSocket.Server;

// Yes, TLS is required
const serverConfig = {
  key: fs.readFileSync(process.env.CERTIFICATE_KEY_FILE || 'key.pem'),
  cert: fs.readFileSync(process.env.CERTIFICATE_CERT_FILE || 'cert.pem'),
};

let webSockets = new Map();
// ----------------------------------------------------------------------------------------

const httpsServer = https.createServer(serverConfig);
httpsServer.listen(HTTPS_PORT);

// ----------------------------------------------------------------------------------------

// Create a server for handling websocket calls
const wss = new WebSocketServer({ server: httpsServer });

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST || '127.0.0.1',
  port: process.env.REDIS_PORT || 6379
});

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);
let iceServers = [];

client.tokens.create().then(token => {
  iceServers = token.iceServers || [];
});

redisClient.on('connect', () => {
  console.log('Redis connect.');
});

redisClient.on('error', (error) => {
  console.error(error);
});

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    // Broadcast any received message to all clients
    console.log('received: %s', message);

    let messageObj = JSON.parse(message);

    const roomId = (messageObj.roomId) ? wss.cryptoText(messageObj.roomId) : null;
    const userId = (messageObj.uuid) ? wss.cryptoText(messageObj.uuid) : null;
    const join = messageObj.join || false;
    const dest = (messageObj.dest) ? wss.cryptoText(messageObj.dest) : null;

    if (join === true && roomId !== null && userId !== null) {
      ws.on('close', () => {
        // Close and clean current room if there are not users connected to room
        console.log('Client close: %s Room %s', userId, roomId);
        wss.cleanRoom(roomId, userId);
      });

      // Index connection with userId
      webSockets.set(userId, ws);

      // Add user to room
      redisClient.sadd(roomId, userId);
      // Send the iceServers to client
      messageObj.iceServers = iceServers;
      ws.send(JSON.stringify(messageObj));
    }

    if (dest !== null &&
        dest !== "all" &&
        dest !== userId &&
        webSockets.has(dest) &&
        webSockets.get(dest).readyState === WebSocket.OPEN &&
        redisClient.sismember(roomId, dest) > 0
    ) {
      webSockets.get(dest).send(message);

    } else {
      wss.broadcast(roomId, userId, message);
    }
  });

  ws.on('error', () => ws.terminate());
});

wss.cryptoText = (text) => {
  let cipher = crypto.createCipheriv(
    cryptoConfig.algorithm,
    Buffer.from(cryptoConfig.key),
    cryptoConfig.iv
  );

  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return encrypted.toString('hex');
};

wss.cleanRoom = (roomId, userId) => {
  if (webSockets.has(userId)) {
    if (webSockets.get(userId).readyState !== WebSocket.CLOSED) {
      webSockets.get(userId).terminate();
    }

    webSockets.delete(userId);
  }

  // remove user from key room member
  redisClient.srem(roomId, userId);
};

wss.broadcast = (roomId, currentUserClientID, data) => {
  redisClient.smembers(roomId, (err, room) => {
    if (err) {
      console.log(err);
      return;
    }

    room.forEach((userId) => {
      if (webSockets.has(userId)) {
        if (webSockets.get(userId).readyState === WebSocket.OPEN && currentUserClientID !== userId) {
          webSockets.get(userId).send(data);
        }

        if (webSockets.get(userId).readyState === WebSocket.CLOSED) {
          webSockets.get(userId).terminate();
          webSockets.delete(userId);
        }
      } else {
        webSockets.delete(userId);
      }
    });
  });
};

console.log('Server running.');

// ----------------------------------------------------------------------------------------
